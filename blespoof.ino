#include <ArduinoBLE.h>

#define ENABLE_PIN 2
#define CONTROL_PIN 3

float power = 0;
float minPower = 0.3;
float maxPower = 1.0;

int isEnabled = LOW;


// helper functions

void setPower(float powerValue) {
  if(powerValue > 0.05) {
    powerValue = powerValue * (maxPower - minPower) + minPower; // scale between min and max power
    power = min(powerValue, maxPower);
    analogWrite(CONTROL_PIN, (int)(power * 255));
  } else { // clamp low power values to 0
    power = 0;
    analogWrite(CONTROL_PIN, 0);
  }
}

void setEnabled(bool enable) {
  isEnabled = enable ? HIGH : LOW;
  digitalWrite(ENABLE_PIN, isEnabled);
}

// UUIDs pulled directly from STPIHKAL
BLEService motorService("00001523-1212-efde-1523-785feabcd123");
BLECharacteristic enableChar("00001524-1212-efde-1523-785feabcd123", BLERead |  BLEWrite | BLEWriteWithoutResponse, 2);
BLECharacteristic speedChar("00001526-1212-efde-1523-785feabcd123", BLERead | BLEWrite | BLEWriteWithoutResponse, 2);
BLECharacteristic tempChar("00001527-1212-efde-1523-785feabcd123", BLERead | BLEWrite | BLEWriteWithoutResponse, 2);

// event handlers

void motorSpeedWritten(BLEDevice central, BLECharacteristic characteristic) {
  const byte& powerValue = *speedChar.value();
  Serial.print("P: ");
  Serial.println(powerValue/255.0);
  setPower(powerValue/255.0);
}

void motorEnableWritten(BLEDevice central, BLECharacteristic characteristic) {
  const byte& modeValue = *enableChar.value();
  Serial.print("E: ");
  Serial.println(modeValue, HEX);
  setEnabled(modeValue == 0x03);
}


void setup() {
  pinMode(ENABLE_PIN, OUTPUT);
  pinMode(CONTROL_PIN, OUTPUT);
  pinMode(LED_BUILTIN, OUTPUT);

  Serial.begin(9600);

  if(!BLE.begin()) {
    Serial.println("BLE Init Error");
    digitalWrite(LED_BUILTIN, HIGH);
    while(true); // halt
  }

  BLE.setLocalName("Vibratissimo");

  // Initialise BLE services
  BLE.setAdvertisedService(motorService);

  motorService.addCharacteristic(enableChar);
  motorService.addCharacteristic(speedChar);
  motorService.addCharacteristic(tempChar);
  BLE.addService(motorService);

  // setup event handlers
  enableChar.setEventHandler(BLEWritten, motorEnableWritten);
  speedChar.setEventHandler(BLEWritten, motorSpeedWritten);

  Serial.println("BLE Init Success");
  BLE.advertise(); // start advertising BLE service
}

void loop() {
  BLEDevice central = BLE.central();
  
  if (central) {
    // device connected
    Serial.println("Central connected");
    Serial.println(central.address());
    digitalWrite(LED_BUILTIN, HIGH);

    while (central.connected())
      BLE.poll(); // poll events for event handlers
    
    // device disconnected
    Serial.println("Central disconnected");
    Serial.println(central.address());
    digitalWrite(LED_BUILTIN, LOW);
    
    // reset device state
    setEnabled(false);
    setPower(0);
    enableChar.writeValue((u_int16_t) 0);
    speedChar.writeValue((u_int16_t) 0);
  }
  
  delay(25);
}
