# Arduino BLE Service Spoofing

By emulating (spoofing) the behaviour of a [Vibratissimo](https://stpihkal.docs.buttplug.io/hardware/vibratissimo.html#bluetooth-details) I was able to make an Arduino work with the existing Buttplug server.

Outputs a PWM signal on pin 3 and enable signal on pin 2. This only works for single-motor applications, although you can try to look through the repo and find other services to spoof that match your desired application.

Works with all boards supported by `ArduinoBLE` (Nano 33 BLE, Nano 33 IoT, Uno WiFi Rev 2, MKR WiFi 1010, etc)
